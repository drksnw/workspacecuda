Membres du groupe:
	- Guillaume Petitpierre

TP réalisés:
	- Rippling (OMP et CUDA)
	- Mandelbrot (OMP et CUDA)
	- Raytracing (CUDA)
	- Projet Démo (CUDA)
