#include "Indice2D.h"
#include "cudaTools.h"
#include "Device.h"

#include "IndiceTools_GPU.h"

#include "RaytracingMath.h"

#include "length_cm.h"

using namespace gpu;

__constant__ Sphere TAB_CM[LENGTH_CM];

__global__ void raytracingGM(uchar4* ptrDevPixels, Sphere* ptrDevTabSphere, uint w, uint h, uint nbSphere, float t);
__global__ void raytracingCM(uchar4* ptrDevPixels, uint w, uint h, float t);
__device__ void work(uchar4* ptrDevPixels, Sphere* ptrDevTabSphere, uint w, uint h, uint nbSphere, float t);


__device__ void copyGMtoSM(Sphere* ptrDevTabGM, Sphere* ptrDevTabSM, int n)
    {
    const int TID_LOCAL = Indice2D::tidLocal();
    const int NB_THREAD_LOCAL = Indice2D::nbThreadLocal();
    }

__host__ void uploadGPU(Sphere* tabValue)
    {
    size_t size = LENGTH_CM * sizeof(Sphere);
    int offset = 0;
    HANDLE_ERROR(cudaMemcpyToSymbol(TAB_CM, tabValue, size, offset, cudaMemcpyHostToDevice));
    }

__global__ void raytracingGM(uchar4* ptrDevPixels, Sphere* ptrDevTabSphere, uint w, uint h, uint nbSphere, float t)
    {
    work(ptrDevPixels, ptrDevTabSphere, w, h, nbSphere, t);
    }

__global__ void raytracingCM(uchar4* ptrDevPixels, uint w, uint h, float t)
    {
    work(ptrDevPixels, TAB_CM, w, h, LENGTH_CM, t);
    }

__device__ void work(uchar4* ptrDevPixels, Sphere* ptrDevTabSphere, uint w, uint h, uint nbSphere, float t)
    {
    RaytracingMath raytracingMath(ptrDevTabSphere, nbSphere);
    const int TID = Indice2D::tid();
    const int NB_THREAD = Indice2D::nbThread();
    const int WH = w * h;

    int s = TID;
    int i;
    int j;

    while (s < WH)
	{
	IndiceTools::toIJ(s, w, &i, &j);
	raytracingMath.colorXY(&ptrDevPixels[s], i, j, t);
	s += NB_THREAD;
	}
    }
