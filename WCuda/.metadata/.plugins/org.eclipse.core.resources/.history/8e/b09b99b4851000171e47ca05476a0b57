#include "Device.h"
#include "Raytracing.h"

#include "length_cm.h"

extern __global__ void raytracingGM(uchar4* ptrDevPixels, Sphere* ptrDevTabSphere, uint w, uint h, uint nbSphere, float t);
extern __global__ void raytracingCM(uchar4* ptrDevPixels, uint w, uint h, float t);
extern __global__ void raytracingSM(uchar4* ptrDevPixels, Sphere* ptrDevTabSphere, uint w, uint h, uint nbSphere, float t);

extern __host__ void uploadGPU(Sphere* tabValue);

Raytracing::Raytracing(const Grid& grid, uint w, uint h, float dt, int nbSpheres) :
	Animable_I<uchar4>(grid, w, h, "Raytracing_Cuda_RGBA_uchar4")
    {
    this->dt = dt;
    this->nbSpheres = nbSpheres;
    this->sizeOctet = nbSpheres * sizeof(Sphere);

    this->t = 0;

    SphereCreator sphereCreator(nbSpheres, w, h);
    Sphere* ptrTabSphere = sphereCreator.getTabSphere();

    //Transfer to GM
    Device::malloc(&ptrDevTabSphere, sizeOctet);
    Device::memclear(ptrDevTabSphere, sizeOctet);
    Device::memcpyHToD(ptrDevTabSphere, ptrTabSphere, sizeOctet);

    //Transfer to CM
    uploadGPU(ptrTabSphere);

    }

Raytracing::~Raytracing()
    {
    Device::free(ptrDevTabSphere);
    }

void Raytracing::animationStep()
    {
    t += dt;
    }

void Raytracing::process(uchar4* ptrDevPixels, uint w, uint h, const DomaineMath& domaineMath)
    {
    Device::lastCudaError("raytracing before");
    static int i = 0;
    if (i%3 == 0)
	{
	//Using GM Kernel
	raytracingGM<<<dg, db>>>(ptrDevPixels, ptrDevTabSphere, w, h, this->nbSpheres, t);
	}
    else if (i%3 == 1)
	{
	//Using CM Kernel
	raytracingCM<<<dg, db>>>(ptrDevPixels, w, h, t);
	}
    else if (i%3 == 2)
	{
	//Using SM Kernel
	raytracingSM<<<dg, db, sizeOctet>>>(ptrDevPixels, ptrDevTabSphere, w, h, this->nbSpheres, t);
	}
    i++;
    Device::lastCudaError("raytracing after");
    }
