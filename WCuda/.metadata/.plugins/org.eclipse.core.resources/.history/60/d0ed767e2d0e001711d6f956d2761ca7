#include "Indice2D.h"
#include "cudaTools.h"
#include "Device.h"

#include "IndiceTools_GPU.h"

#include "RaytracingMath.h"

#include "length_cm.h"

using namespace gpu;

__constant__ Sphere TAB_CM[LENGTH_CM];

__global__ void raytracingGM(uchar4* ptrDevPixels, Sphere* ptrDevTabSphere, uint w, uint h, uint nbSphere, float t);
__device__ void work(uchar4* ptrDevPixels, Sphere* ptrDevTabSphere, uint w, uint h, uint nbSphere, float t);

__global__ void raytracingGM(uchar4* ptrDevPixels, Sphere* ptrDevTabSphere, uint w, uint h, uint nbSphere, float t)
    {
    work(ptrDevPixels, ptrDevTabSphere, w, h, nbSphere, t);
    }

__device__ void work(uchar4* ptrDevPixels, Sphere* ptrDevTabSphere, uint w, uint h, uint nbSphere, float t)
    {
    RaytracingMath raytracingMath(ptrDevTabSphere, nbSphere);
    const int TID = Indice2D::tid();
    const int NB_THREAD = Indice2D::nbThread();
    const int WH = w * h;

    int s = TID;
    int i;
    int j;

    while (s < WH)
	{
	IndiceTools::toIJ(s, w, &i, &j);
	raytracingMath.colorXY(&ptrDevPixels[s], i, j, t);
	s += NB_THREAD;
	}
    }
